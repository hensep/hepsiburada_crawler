module.exports = function(sequelize, DataTypes) {
    var table =  sequelize.define('db_crawler', {
        url: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 250]
            }
        },
        image_name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 250]
            }
        },
        price: {
            type: DataTypes.DECIMAL,
            allowNull: false
        },
        is_crawled: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });

    sequelize.sync({force: true}).then(function () {
        console.log('database created..');
    });
    return table;
};