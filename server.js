var express     = require('express');
var fs          = require('fs');
var request     = require('request');
var cheerio     = require('cheerio');
var bodyParser  = require('body-parser');
var validator   = require('validator');
var app         = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())


var Sequelize   = require('sequelize');
var sequelize   = new Sequelize(undefined, undefined, undefined , {
    'dialect'   : 'sqlite',
    'storage'   : __dirname + "/data/hb_crawler.sqlite"
});

var table       = require('./models/db_crawler.js');
var db_table    = table(sequelize,Sequelize);

app.get('/', function (req, res) {
    fs.readFile("index.html",function (err,html) {
        res.writeHead(200, {'Content-Type': 'text/html','Content-Length':html.length});
        res.write(html);
        res.end();
    });
});



app.post('/crawler', function(req, res){

    var url = req.body.url;

    if (!validator.isURL(url)) { // if url address is not validate..
        res.write('Url is not valid address!');
        res.end();
    } else {

        // request get url address..
        request(url, function(error, response, html){

            var $ = cheerio.load(html);
            var price;
            var price_array = new Array();

            if(!error){

                // get product price..
                $('[itemprop=price] span').filter(function(){
                    price = $(this).text();

                    if (price_array.length < 2) {
                        price_array.push(price);
                    }
                });


                // get product image url..
                var image = null;
                $("#productDetailsCarousel a").first().filter(function () {
                    image = $(this).find("img").attr("data-src");
                });

                var image_url = 'http:' + image;
                var split_url = image_url.split('/');

                // set the filename..
                var filename = split_url[split_url.length -1];
                var full_path = './upload/' + filename;

                // set the product price in array..
                var price_double = price_array[0] + "." + price_array[1];

                res.setHeader('Content-Type', 'text/html');
                res.writeHead(res.statusCode);

                // image download and write disk..
                request(image_url).pipe(fs.createWriteStream(full_path)).on('close', function () {
                    console.log("image downloaded.. "+ image_url);
                });

                // data insert database..
                db_table.create({
                    'image_name'    : filename,
                    'url'           : image_url,
                    'price'         : parseFloat(price_double),
                    'is_crawled'    : true

                }).then(function () {
                    console.log("database inserted a row..");
                });

                // response display html..
                res.write('<img src="http://' + image + '"/><br/><br/><strong>Price</strong> : '+ price_array[0] + "." + price_array[1] + " TL");
                res.end();
            }
        });
    }

});


// app listening..
app.listen(3000);
console.log('Application starting on port 3000!');
console.log('This crawler -> http://localhost:3000/');
exports = module.exports = app;